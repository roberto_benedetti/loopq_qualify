import torch
from torch import nn
import torch.nn.functional as F

class nn_linear_4(nn.Module):
    def __init__(self):
        super(nn_linear_4, self).__init__()
        '''BEST SO FAR 0.65
        self.encoder = nn.Linear(4,50)
        self.hid1 =  nn.Linear(50,100)
        self.drop1 = nn.Dropout(0.4)
        self.hid2 =  nn.Linear(100,100)
        self.drop2 = nn.Dropout(0.4)
        self.hid3 =  nn.Linear(100,50)
        self.drop3 = nn.Dropout(0.4)
        self.decoder = nn.Linear(50,2)
        '''
        self.encoder = nn.Linear(4,50)
        self.hid1 =  nn.Linear(50,100)
        self.drop1 = nn.Dropout(0.4)
        self.hid2 =  nn.Linear(100,200)
        self.drop2 = nn.Dropout(0.4)
        self.hid3 =  nn.Linear(200,100)
        self.drop3 = nn.Dropout(0.4)
        self.decoder = nn.Linear(100,2)
        # self.softmax = nn.Softmax(dim=1)
        print("Number of parameters Linear4: ", self.count_parameters())


    def forward(self, x):
        x = self.encoder(x)
        x = F.relu(x)
        x = self.hid1(x)
        x = F.relu(x)
        x = self.drop1(x)
        x = self.hid2(x)
        x = F.relu(x)
        x= self.drop2(x)
        x = self.hid3(x)
        x = F.relu(x)
        x= self.drop3(x)
        x = self.decoder(x)
        # x = self.softmax(x)
        return x

    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

