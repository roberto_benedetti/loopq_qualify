# Simple script for loading precomputed model and valuate results on a given test set
import numpy as np
import joblib

# PATH TO THE TEST FILE TO EVALUATE
TEST_PATH = '/home/rba/Projects/loopq_qualify/test_data/Scrumpulicious_test.csv'

# LOAD PRE-COMPUTED MODELS
dataload = joblib.load('dataset_linear_4')
model_ = joblib.load('RandomForestClassifier_4')

# TANSFORM TEST DATA AND COMPUTE PREDICTIONS
test_data = dataload.transform_test_data(PATH=TEST_PATH)
predictions = model_.predict(test_data)

print('Test_set count positive(joblib): {}/{}'.format(sum(model_.predict(test_data)),len(test_data)))

# Now compute the final csv for result

final_test_set = dataload.load_test_data(PATH = TEST_PATH)
final_test_set.GREEN = predictions
final_test_set.to_csv('challenge_test_set_labeled.csv',index=None)
