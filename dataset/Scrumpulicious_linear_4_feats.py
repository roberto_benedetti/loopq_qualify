'''Create simple data model for classical analysis using only most correlated feats'''

import numpy as np
from dataset.Scrumpulicious_abstract import Scrumpulicious_abstract

# Fix random generators for reproducitbility
from ml.nn.utils import set_random_numpy
set_random_numpy()
# Set and create save folders

class Scrumpulicious_linear_4(Scrumpulicious_abstract):
    def __init__(self):
        super(Scrumpulicious_linear_4, self).__init__(dataset_name='Scrumpulicious_linear_4')


    def data_preprocesser(self):
        # Still we remove collinear features, but we also remove all features with low correlation with labels

        corr_abs = self.df.corr().abs()

        upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
        print('Rows to remove due to high correlation (>.85)',to_drop)
        #
        self.df = self.df.drop(self.df[to_drop], axis=1)
        print('New dataframe shape:',self.df.shape)

        high_correlated = list(corr_abs['GREEN'][corr_abs['GREEN'] > 0.08].index)

        self.df = self.df[high_correlated]

