
class AbstractSample(object):

    def read_features(self):

        raise ("Not implemented - this is an abstract method")

    def read_labels(self):

        raise ("Not implemented - this is an abstract method")


class Sample(AbstractSample):

    def __init__(self, input_seq, target):
        super(Sample, self).__init__()
        self.input_seq = input_seq
        self.target = target

    def read_features(self):

        return self.input_seq

    def read_labels(self):

        return self.target