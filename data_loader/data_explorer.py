import MLlib
import __root__
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

# Fix random generators for reproducitbility
np.random.seed(777)

path = os.path.join(__root__.path(),'dataset','raw','Scrumpulicious.csv')

df = pd.read_csv(path)
df.TIME = pd.to_datetime(df.TIME)

print('DATASET SPECS:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
      '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df.shape,sum(df.GREEN),
                                               100*sum(df.GREEN)/len(df),
                                               len(df)-sum(df.GREEN),
                                               100*(1-sum(df.GREEN)/len(df))))


# EDA

plt.figure(figsize=(20,10))
plt.title('Green distribution')
plt.plot(df.TIME,np.zeros_like(df.TIME), marker='o',color='yellow',markeredgewidth=.1, markeredgecolor='black', linewidth=0)
plt.plot(df.TIME.loc[df.GREEN>0],np.zeros_like(df.TIME.loc[df.GREEN>0]), marker='o',color ='green', linewidth=0)
plt.xlabel('time')
plt.ylabel('green occurrence')
plt.show()
plt.close()

sns.set(style="ticks", color_codes=True)

# # correlation

corr = df.corr()
corr_abs = df.corr().abs()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.show()

# Some features are higly correlated, we should remove them
upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
print('Rows to remove due to high correlation (>.85)',to_drop)
#
df = df.drop(df[to_drop], axis=1)
print('New dataframe shape:',df.shape)

# Show new correlation matrix
# '''
corr = df.corr()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))
plt.title('New correlation matrix')
# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.show()


# print(corr.loc[])

sns.set(style="ticks", color_codes=True)
g = sns.pairplot(df[['PIB', 'MOZ', 'NOL', 'ZEN', 'SIP', 'MAZ','GREEN']],hue="GREEN",diag_kind='kde'  )
plt.show()


sns.jointplot(x="NOL", y="GREEN", data=df)
plt.show()
# '''
# t-sne visualization

from sklearn.manifold import TSNE

cols = list(df.columns)
y_cols = ['GREEN']
remove_cols = ['TIME']

x_cols = [x for x in cols if x not in y_cols]
x_cols = [x for x in x_cols if x not in remove_cols]

# to improve visualization we collect as many green samples we can, while we resample only a fraction of gold samples
df_green = df.loc[df['GREEN']==1]
df_gold = df.loc[df['GREEN']==0]

df_gold = MLlib.shuffleDataframe(df_gold)
df_gold = df_gold[0:102]

df = pd.concat([df_gold,df_green], axis=0)
df = MLlib.shuffleDataframe(df)

print('RE-Sampled dataframe for t-sne visualization:',df.shape)

green = df['GREEN'] == 1
gold = df['GREEN'] == 0
df_x = df[x_cols]
print('number of positives:', sum(green))
tsne = TSNE(n_components=2,verbose=1, random_state=999, perplexity=20)
x_embedded = tsne.fit_transform(X=df_x)

fig=plt.figure()
ax = fig.add_subplot(111)
# ax = fig.add_subplot(111, projection='3d')
plt.scatter(x_embedded[green,0],x_embedded[green,1], color = 'green')
plt.scatter(x_embedded[gold,0],x_embedded[gold,1], color = 'yellow')
plt.show()


