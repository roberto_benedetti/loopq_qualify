import pandas as pd
import numpy as np
from sklearn.metrics import f1_score, accuracy_score
# from sklearn.metrics.scorer import f1_scorer
from MLlib import model_scorer
from data_loader.Scrumpulicious import Scrumpulicious_linear_4

from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC,SVC
from sklearn.neural_network import MLPClassifier
import joblib

from ml.nn.utils import set_random_numpy

set_random_numpy()

dataset = Scrumpulicious_linear_4()
dataset.read()

print(dataset.train[0].shape)

# model = LogisticRegression(C=10,class_weight={0:0.7,1:1.0} )
# model = LogisticRegressionCV(cv = 10 , scoring='f1', verbose=1, n_jobs=-1)
# model = LinearSVC(class_weight={0:.7,1:1.0},C=0.04)
# model = SVC(C=30,gamma =1.0,class_weight={0:0.7,1:1.0})
model = RandomForestClassifier(criterion='entropy',max_features=2,max_depth=6,n_estimators=70,class_weight={0:0.7,1:1.0},n_jobs=-1,random_state=555)
# model = LinearDiscriminantAnalysis()
# model = QuadraticDiscriminantAnalysis()
# model = MLPClassifier(hidden_layer_sizes=(100,200,200),early_stopping=False,verbose=1)

######################
# model for cross with random split on 999
#
# model = RandomForestClassifier(criterion='entropy',max_features=2,max_depth=6,n_estimators=70,class_weight={0:0.7,1:1.0},n_jobs=-1,random_state=555)
#
# params_dict = {'random_state':[555],
#                'max_features': np.arange(2,5,1),
#                'max_depth': [6,7,8,9,10,11],
#                # 'n_estimators':[50,60,70,80,90]
#                }



from sklearn.model_selection import GridSearchCV
params_dict = {'C':[0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01,0.03,0.05,0.1,0.3,0.5,1.0,3.0,5.0,10,30,50],
               'gamma':[0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01,0.03,0.05,0.1,0.3,0.5,1.0,3.0,5.0,10,30,50]}

# params_dict = {'C':[0.5,1.0,3.0,5.0,10,30,50]}
# params_dict = {'n_components':np.arange(1,48)}

# RandomForest params
# params_dict = { 'random_state':[555,1,2,3,4,5,6],
                # 'max_depth':np.arange(2,10,1),
                # 'max_features':np.arange(1,5,1),
                # 'n_estimators': [50,60,70,80,90]
                # }
# SVM GRID RBF
# params_dict = {'C':np.linspace(10,30,num=30),
#                'gamma':np.linspace(0.005,0.007,num=2)}

grid = GridSearchCV(model,param_grid=params_dict,scoring = 'f1' ,n_jobs=-1,cv=4,verbose=1)
#
grid = model

grid.fit(dataset.train[0],dataset.train[1])
#
# print(grid.best_params_)

model_scorer(dataset = dataset, model = grid)

# Dump models for future use
# joblib.dump(dataset, 'dataset_linear_4')
# joblib.dump(model,'RandomForestClassifier_4')

# Test of VALUATION OF TEST SET
#
# dataload = joblib.load('dataset_linear_4')
# model_ = joblib.load('RandomForestClassifier_4')
#
# # Using script stuff
# test_data = dataset.transform_test_data()
# predictions = grid.predict(test_data)
# # Using joblib import
# test_data2 = dataload.transform_test_data()
# predictions2 = model_.predict(test_data2)
# predictions_te = model_.predict(dataload.test[0])
# print('Do solution match?',np.all(predictions==predictions2))
# print('Do solution fo test match?',np.all(predictions_te==grid.predict(dataset.test[0])))
#
# print('Test_set count positive: {}/{}'.format(sum(grid.predict(dataset.test[0])),len(dataset.test[0])))
# print('Test_set count positive(joblib): {}/{}'.format(sum(model_.predict(dataload.test[0])),len(dataload.test[0])))
# print('Final test count positive: {}/{}'.format(sum(grid.predict(test_data)),len(test_data)))
# print('Fina test set count positive(joblib): {}/{}'.format(sum(model_.predict(test_data2)),len(test_data2)))
# # VALUATION OF TEST SET
#
# test_data = dataset.transform_test_data()
# predictions = grid.predict(test_data)

