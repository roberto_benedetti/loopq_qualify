'''Create a model collecting features from previous samples'''
from datetime import timedelta

import numpy as np
import pandas as pd

# Fix random generators for reproducitbility
from numpy.core.multiarray import result_type

from dataset.Scrumpulicious_abstract import Scrumpulicious_abstract
from ml.nn.utils import set_random_numpy

set_random_numpy()

# Set and create save folders

class Scrumpulicious_time_collector(Scrumpulicious_abstract):
    def __init__(self, minutes:int):
        super(Scrumpulicious_time_collector,self).__init__(dataset_name='Scrumpulicious_time_collector')
        self.minutes = minutes
        # Tollerance parameter of number of producted bars within the time range for building statistical features
        self.tol =  np.int(np.floor((self.minutes//2-0.2*self.minutes//2)))

    def data_preprocesser(self):
        # We should remove High collinear features as seen in data_explorer

        corr_abs = self.df.corr().abs()

        upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
        print('Rows to remove due to high correlation (>.85)',to_drop)
        #
        self.df = self.df.drop(self.df[to_drop], axis=1)
        print('New dataframe shape:',self.df.shape)

        self.time_collector(minutes=self.minutes)

    def time_collector(self,minutes:int):
        '''Collect statistical features from past minutes'''
        delta = timedelta(minutes=minutes)
        cols_to_augment = ['PIB', 'MOZ', 'NOL', 'ZEN', 'SIP', 'MAZ', 'FOK', 'BIN', 'SIG',
         'ZEV', 'ZEL', 'VOV', 'SAM', 'ZEZ', 'ZET', 'DER', 'REL', 'DAG', 'NOK',
         'BON', 'SAS', 'KEF', 'KAK', 'RIG', 'ROD', 'NEM', 'NOZ', 'ZEP', 'KIB',
         'TOB', 'FAG', 'LEZ', 'RUM', 'SOV', 'ZIK', 'LAS', 'FET', 'KUT', 'BIV',
         'PAD', 'SAP', 'RAR', 'SIM', 'DUB', 'ZIR', 'RUP', 'GIS', 'SUG']

        def collect(x,self=self):
            time_start = x.TIME - delta
            select_df = self.df.loc[(self.df.TIME<x.TIME) & (self.df.TIME>=time_start)]
            select_df = select_df[cols_to_augment]
            # If there are less than # tol bars, discard the row
            if len(select_df)>=self.tol:
                means = select_df.mean()
                std = select_df.std()

                x = x.append(means)
                x =  x.append(std)
                return x

        # df = self.df[0:50]
        df = self.df


        means_cols = [x + '_avg' for x in cols_to_augment]
        std_cols= [x + '_std' for x in cols_to_augment]
        new_cols = list(df.columns) + means_cols + std_cols
        data= df.apply(collect, axis=1)
        data = pd.DataFrame(list(data.dropna()), columns=new_cols)

        self.df = data

        # We should check again for possibile collinearity since we added a lot of features
        corr_abs = self.df.corr().abs()

        upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
        print('Rows to remove due to high correlation (>.85)', to_drop)
        #
        self.df = self.df.drop(self.df[to_drop], axis=1)
        print('New dataframe shape:', self.df.shape)


