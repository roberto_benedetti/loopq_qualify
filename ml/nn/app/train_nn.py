from datetime import datetime


from ml.nn.config import get_config
from ml.nn.trainer import Trainer

lr = 0.0001
batch_size = 512
# batch_size = 1024


if __name__ == "__main__":
    config, unparsed = get_config()
    # config.dataset='resample'
    config.dataset='linear_4'
    # config.dataset='classical'
    config.dataset='time_collector'
    #
    # Scale lr proportionally to batch size as shown in:
    # "DON’TDECAY THE LEARNINGRATE,INCREASE THEBATCHSIZE" Samuel L. Smith∗, Pieter-Jan Kindermans∗, Chris Ying & Quoc V. Le
    config.learning_rate = lr * batch_size
    #
    config.num_epochs = 500
    config.weight_decay =1e-4
    config.batch_size=batch_size
    #
    # config.stepLR = 150


    config.log_name = 'dataset_{}_lr_{}_batch_{}_'.format(config.dataset,config.learning_rate,config.batch_size)
    trainer = Trainer(config)
    trainer.start_train()