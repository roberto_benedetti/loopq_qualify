# Classes for ready-to-go machine learning datasets already divided into training/test by dataset package

import __root__
import numpy as np
import pandas as pd
import abc,os

class dataset(abc.ABC):
    def __init__(self):
        print('Dataset:' + self.__class__.__name__)
        self.train = None
        self.test = None

        self.mean = None
        self.std  = None

    # Abstract method
    def read(self):
        pass

    def standardize(self, data:pd.DataFrame):
        return (data-self.mean)/self.std

    def load_test_data(self, PATH:str):
        test_data = pd.read_csv(PATH)
        test_data.TIME = pd.to_datetime(test_data.TIME)
        return test_data

    def transform_test_data(self,PATH:str='/home/rba/Projects/loopq_qualify/test_data/Scrumpulicious_test.csv'):
        '''Compute dataset transformation for a provided test dataset, this is and abstract method'''
        pass



class Scrumpulicious_linear(dataset):
    def __init__(self):
        super(Scrumpulicious_linear).__init__()
        self.PATH = os.path.join(__root__.path(),'dataset','Scrumpulicious_linear')


    def read(self):
        df_tr = pd.read_csv(os.path.join(self.PATH, 'train', 'train.csv'), sep=';')
        df_te = pd.read_csv(os.path.join(self.PATH, 'test', 'test.csv'), sep=';')

        df_tr.TIME = pd.to_datetime(df_tr.TIME)
        df_te.TIME = pd.to_datetime(df_te.TIME)
        cols = list(df_tr.columns)
        y_cols = ['GREEN']
        remove_cols = ['TIME']

        x_cols = [x for x in cols if x not in y_cols]
        x_cols = [x for x in x_cols if x not in remove_cols]

        df_tr_x = df_tr[x_cols]
        df_te_x = df_te[x_cols]

        df_tr_y = df_tr[y_cols]
        df_te_y = df_te[y_cols]

        self.mean = df_tr_x.mean()
        self.std = df_tr_x.std()

        self.train = [self.standardize(df_tr_x).values, df_tr_y.values.ravel()]
        self.test  = [self.standardize(df_te_x).values, df_te_y.values.ravel()]


class Scrumpulicious_resample(dataset):
    def __init__(self):
        super(Scrumpulicious_resample).__init__()
        self.PATH = os.path.join(__root__.path(),'dataset','Scrumpulicious_resample')


    def read(self):
        df_tr = pd.read_csv(os.path.join(self.PATH, 'train', 'train.csv'), sep=';')
        df_te = pd.read_csv(os.path.join(self.PATH, 'test', 'test.csv'), sep=';')

        cols = list(df_tr.columns)
        y_cols = ['GREEN']

        x_cols = [x for x in cols if x not in y_cols]

        df_tr_x = df_tr[x_cols]
        df_te_x = df_te[x_cols]

        df_tr_y = df_tr[y_cols]
        df_te_y = df_te[y_cols]

        self.mean = df_tr_x.mean()
        self.std = df_tr_x.std()

        self.train = [self.standardize(df_tr_x).values, df_tr_y.values.ravel()]
        self.test  = [self.standardize(df_te_x).values, df_te_y.values.ravel()]

        print('Train Dataset Shape', self.train[0].shape)
        print('Test Dataset Shape', self.test[0].shape)

class Scrumpulicious_linear_4(dataset):
    def __init__(self):
        super(Scrumpulicious_linear_4).__init__()
        self.PATH = os.path.join(__root__.path(),'dataset','Scrumpulicious_linear_4')


    def read(self):
        df_tr = pd.read_csv(os.path.join(self.PATH, 'train', 'train.csv'), sep=';')
        df_te = pd.read_csv(os.path.join(self.PATH, 'test', 'test.csv'), sep=';')

        # df_tr.TIME = pd.to_datetime(df_tr.TIME)
        # df_te.TIME = pd.to_datetime(df_te.TIME)
        cols = list(df_tr.columns)
        y_cols = ['GREEN']

        x_cols = [x for x in cols if x not in y_cols]

        df_tr_x = df_tr[x_cols]
        df_te_x = df_te[x_cols]

        df_tr_y = df_tr[y_cols]
        df_te_y = df_te[y_cols]

        self.mean = df_tr_x.mean()
        self.std = df_tr_x.std()

        self.train = [self.standardize(df_tr_x).values, df_tr_y.values.ravel()]
        self.test  = [self.standardize(df_te_x).values, df_te_y.values.ravel()]

        self.x_cols = x_cols

    # Override
    def transform_test_data(self, PATH:str='/home/rba/Projects/loopq_qualify/test_data/Scrumpulicious_test.csv'):
        test_data = self.load_test_data(PATH)
        # For this dataset structure we just need to discard all features except for the most correlated ones
        test_data = test_data[self.x_cols]
        # Now we can compute normalization using the training means and std
        test_data = self.standardize(test_data).values
        return test_data


class Scrumpulicious_resample_4(dataset):
    def __init__(self):
        super(Scrumpulicious_resample_4).__init__()
        self.PATH = os.path.join(__root__.path(),'dataset','Scrumpulicious_resample_4')


    def read(self):
        df_tr = pd.read_csv(os.path.join(self.PATH, 'train', 'train.csv'), sep=';')
        df_te = pd.read_csv(os.path.join(self.PATH, 'test', 'test.csv'), sep=';')

        cols = list(df_tr.columns)
        y_cols = ['GREEN']

        x_cols = [x for x in cols if x not in y_cols]

        df_tr_x = df_tr[x_cols]
        df_te_x = df_te[x_cols]

        df_tr_y = df_tr[y_cols]
        df_te_y = df_te[y_cols]

        self.mean = df_tr_x.mean()
        self.std = df_tr_x.std()

        self.train = [self.standardize(df_tr_x).values, df_tr_y.values.ravel()]
        self.test  = [self.standardize(df_te_x).values, df_te_y.values.ravel()]

class Scrumpulicious_time_collector(dataset):
    def __init__(self):
        super(Scrumpulicious_time_collector).__init__()
        self.PATH = os.path.join(__root__.path(),'dataset','Scrumpulicious_time_collector')
        print('LOADED TIME COLLECTOR DATASET')


    def read(self):
        df_tr = pd.read_csv(os.path.join(self.PATH, 'train', 'train.csv'), sep=';')
        df_te = pd.read_csv(os.path.join(self.PATH, 'test', 'test.csv'), sep=';')

        cols = list(df_tr.columns)
        y_cols = ['GREEN']
        remove_cols = ['TIME']

        x_cols = [x for x in cols if x not in y_cols]
        x_cols = [x for x in x_cols if x not in remove_cols]


        df_tr_x = df_tr[x_cols]
        df_te_x = df_te[x_cols]

        df_tr_y = df_tr[y_cols]
        df_te_y = df_te[y_cols]

        self.mean = df_tr_x.mean()
        self.std = df_tr_x.std()

        self.train = [self.standardize(df_tr_x).values, df_tr_y.values.ravel()]
        self.test  = [self.standardize(df_te_x).values, df_te_y.values.ravel()]