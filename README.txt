Code for the qualifying challange by Roberto Benedetti

The code is still a little bit messy since I haven't got time to clean it, however in this readme there's a small explaination
of the design implemented for computing the task and the involved classes.

INDEX:

1- Code overview
2- In depth packages overview
	2.1-dataset
	2.2-data_loader
	2.3-ml
3- Model evaluation

-----------------------------------------------------------

					1- Code Overview

------------------------------------------------------------
The project is divided in 3 main packages:
1- dataset: which has several scripts for computing different datasets
2- data_loader: which contains the classes of the actual dataset 	used by machine learning algorithms (dataset must be pre-computed
	from the previous package)
3- ml: containing the machine learning scripts working with data classes provided by data_loader package

So there is a dependency in usage the following code:
dataset -> data_loader -> ml
-----------------------------------------------------------

				2- In depth packages overview
					
------------------------------------------------------------
2.1 dataset

Several different datasets were computed for the challange, since the implementations are quite similar the class 'Scrumpulicious_abstract'
provides a general implementation of the building algorithm.
The building is done with a template design patter through the method "template_processer" which has 3 steps:
-data_processer: which loads raw data and computes transformations
-data_random_split: which computes a random split in training and test set
-save_dataset: which saves the computed training and test set in a dedicated folder in the same package with a proper name

A basic implementation of each method is already provided in the abstract class, but any different dataset can be implemented just by
overriding any of the 3.

The usage of the classes is done by a main script called: "dataset_builder_main.py".

The provided concrete classes are:
-Scumpulicious_linear: A linear loading of the raw data with only removal of collinear features
-Scrumpulicious_linear_4_feats: The same as the previous but by keeping only the remaining features with |Pearson_coeff|>0.08 with the GREEN label
-Scrumpulicious_resample: The same as linear, but the training set is then re-sampled using a SMOTE technique for balancing the classes. Note 
							that the test set remains the same as the linear dataset.
-Scrumpulicious_resample_4: Resampled dataset built on linear_4
-Scrumpulicious_time_collector: A dataset made with time-related features. For each sample all data points with a timestamp within a specified
								amount of minutes before are collected. If the data collected are < 6 the sample is discarded. The dataset is 
								then augmented with the means and stds of the previous sample's features. Note that the dataset made with this
								approach is different from the previous ones since we can't compute a fully random split: first time-series features
								must be collected and the random splitting can be computed. This makes valuating this dataset harder.

2.2 dataloader
This package contains the class loaders for the machine learning part.
A basic dataset implementation is made by the abstract class "dataset". 
The classes provide also a method for normalizing features by standardization and a method for preprocessing an input test file with the same procedure
of the training set (which includes the standardization using the means and stds from the training set).

There also two scripts in this package: exploratory_analysis.py, exploratory_analysis_4.py. These script consists in a series of methods for extracting
informations from the data and compute some visualization. The 2nd scripts consists in a in-depth analysis of the dataset with only the 4 most
correlated features with the GREEN label. This was made to improve the visualization of the dataset, and actually resulted so far in the best dataset 
structure for learning techniques. The original idea was to start from 4 features and then trying to add new ones for improving the model.
Since the lack of time this attempt wasn't tested (testing each possibile combination of features results in an exponential number of attempts). 

2.3 ml
The ml package contains the actual scripts computing the machine learning models and evaluating the results.
There are two sub-packages:
-classical: with standard machine learning algorithms
-nn: with deep learning algorithms

In classical there is a script for each tested training dataset from package dataloader, while for nn a torch dataloader was provided for each different
training set.

-----------------------------------------------------------

					3- Model Evaluation					

------------------------------------------------------------

There are two ways for computing test-results:
1) train models with the scripts in the ml package classical/nn and then load test data with dataloader transformation method "transform_test_data(PATH)"
2) use a pre-computed model for both dataloader and ml model

For the second choice, an implementation was provided only for the RandomForestClassifier with the 4-features dataset (due to time).
To use it just run the "dataset_predictor.py" script, which loads a precomputed dataloader and a trained RandomForestClassifier.
The results will be stored in a csv file in the same folder.

