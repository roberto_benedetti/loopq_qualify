from dataset.Scrumpulicious_time_collector import Scrumpulicious_time_collector
from dataset.Scrumpulicious_linear import Scrumpulicious_linear
from dataset.Scrumpulicious_linear_4_feats import Scrumpulicious_linear_4
# from dataset.Scrumpulicious_resample import Scrumpulicious_resample
# from dataset.Scrumpulicious_resample_4 import Scrumpulicious_resample_4
#
from ml.nn.utils import set_random_numpy
set_random_numpy()
# dataset = Scrumpulicious_linear()
# dataset = Scrumpulicious_resample()
dataset = Scrumpulicious_linear_4()
# dataset = Scrumpulicious_resample_4()
# dataset = Scrumpulicious_time_collector(minutes=15)

dataset.template_processer()