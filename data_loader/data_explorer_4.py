# Since from the first analysis we discovered that only few parameters have significant correlation with the label
# it makes sense to plot some visual information with those

import MLlib
import __root__
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

# Fix random generators for reproducibility
np.random.seed(777)

path = os.path.join(__root__.path(),'dataset','raw','Scrumpulicious.csv')

df = pd.read_csv(path)
df.TIME = pd.to_datetime(df.TIME)

corr_abs = df.corr().abs()

upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
print('Rows to remove due to high correlation (>.85)', to_drop)
#
df = df.drop(df[to_drop], axis=1)
print('New dataframe shape:', df.shape)

high_correlated = list(corr_abs['GREEN'][corr_abs['GREEN'] > 0.08].index)

df = df[high_correlated]

print('DATASET SPECS:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
      '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df.shape,sum(df.GREEN),
                                               100*sum(df.GREEN)/len(df),
                                               len(df)-sum(df.GREEN),
                                               100*(1-sum(df.GREEN)/len(df))))


# EDA


sns.set(style="ticks", color_codes=True)

# # correlation

corr = df.corr()
corr_abs = df.corr().abs()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.show()


#
# sns.set(style="ticks", color_codes=True)
# g = sns.pairplot(df,hue="GREEN",diag_kind='kde' )
# plt.show()

# we can plot data distributions with a boxplot

plt.figure()
plt.title('boxplot')
df.boxplot()
plt.show()

# From the boxplot we can see that there are some sparse outliers

# we can easily filter those with a value threshold
# Since we lack of true labels we try to remove

# Outilier removal
to_remove = len(df[(df.abs()>40).any(1)])


print('Number of outilers removed:\n\t',to_remove)
df = df[(df.abs()<40)]


df=df.dropna()

# Now the boxplot will be more clear

plt.figure()
plt.title('boxplot')
df.boxplot()
plt.show()

# Check again dataset distribution to see if we removed any positive label
print('DATASET SPECS AFTEER OUTLIERS REMOVAL:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
      '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df.shape,sum(df.GREEN),
                                               100*sum(df.GREEN)/len(df),
                                               len(df)-sum(df.GREEN),
                                               100*(1-sum(df.GREEN)/len(df))))
# '''

# We can see if there are any correlations through a pairplot


sns.set(style="ticks", color_codes=True)
g = sns.pairplot(df,hue="GREEN",diag_kind='kde' )
plt.show()

# It doesn't look like there are some evident linear dependencies, however points on different classes seems to be
# separable (NOL feature provides a good division)

# We can try to plot a linear separation with a T-SNE dimensionality reduction
# t-sne visualization

from sklearn.manifold import TSNE

cols = list(df.columns)
y_cols = ['GREEN']
remove_cols = ['TIME']

x_cols = [x for x in cols if x not in y_cols]
x_cols = [x for x in x_cols if x not in remove_cols]

# to improve visualization we collect as many green samples we can, while we resample only a fraction of gold samples
df_green = df.loc[df['GREEN']==1]
df_gold = df.loc[df['GREEN']==0]

df_gold = MLlib.shuffleDataframe(df_gold)
df_gold = df_gold[0:200]

df = pd.concat([df_gold,df_green], axis=0)
df = MLlib.shuffleDataframe(df)

print('RE-Sampled dataframe for t-sne visualization:',df.shape)

green = df['GREEN'] == 1
gold = df['GREEN'] == 0
df_x = df[x_cols]
print('number of positives:', sum(green))


tsne = TSNE(n_components=2,verbose=1, random_state=999, perplexity=40 ,  )
x_embedded = tsne.fit_transform(X=df_x)

fig=plt.figure()
plt.title('T-SNE visualization')
ax = fig.add_subplot(111)
# ax = fig.add_subplot(111, projection='3d')
plt.scatter(x_embedded[green,0],x_embedded[green,1], color = 'green', label ='positive')
plt.scatter(x_embedded[gold,0],x_embedded[gold,1], color = 'yellow', label ='negative')
plt.legend()
plt.show()

print()

# With this dimensionality reduction the two classes are not perfectly separable, even trying to play with the params
# of the model, however we can already see that the majority of green labels are grouped together and can be easily
# divided from gold with a linear separating plane allowing some false positives

