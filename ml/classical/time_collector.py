import pandas as pd
import numpy as np
from sklearn.metrics import f1_score, accuracy_score

from MLlib import model_scorer
from data_loader.Scrumpulicious import Scrumpulicious_time_collector
from scipy.stats.stats import pearsonr
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC,SVC
from sklearn.neural_network import MLPClassifier
from sklearn.decomposition import PCA

from ml.nn.utils import set_random_numpy

# warning suppresser
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

set_random_numpy()
#
# def model_scorer(dataset,model):
#     '''Computes various scores'''
#     tr_prediction = model.predict(dataset.train[0])
#     te_prediction = model.predict(dataset.test[0])
#     print('ACCURACY SCORES')
#     print('\ttr:\t{:2.4f}'.format(accuracy_score(y_true=dataset.train[1],y_pred=tr_prediction)))
#     print('\tte:\t{:2.4f}'.format(accuracy_score(y_true=dataset.test[1] , y_pred=te_prediction)))
#     print('F1 SCORES')
#     print('\ttr:\t{:2.4f}'.format(f1_score(pos_label=1,average='macro',y_true=dataset.train[1],y_pred=tr_prediction)))
#     print('\tte:\t{:2.4f}'.format(f1_score(pos_label=1,average='macro',y_true=dataset.test[1] , y_pred=te_prediction)))

dataset = Scrumpulicious_time_collector()
dataset.read()

# pca = PCA(n_components=100)
#
# pca.fit(dataset.train[0])
# dataset.train[0] = pca.transform(dataset.train[0])
# dataset.test[0]  = pca.transform(dataset.test[0])

print(dataset.train[0].shape)



model = LogisticRegression(C=50, class_weight={0:0.7,1:1.0}, penalty='l2')
# model = LinearSVC(C=10, class_weight={0:0.7,1:1.0})
# model = SVC(class_weight={0:0.3,1:1.0}, C=40,gamma=0.001)
# model = RandomForestClassifier(n_estimators= 70, max_features= 70, max_depth=9,
#                                criterion='entropy',class_weight={0:0.7,1:1.0},n_jobs=1,random_state=555) # Crossvalidated
# model = LinearDiscriminantAnalysis()
# model = QuadraticDiscriminantAnalysis()
# model = MLPClassifier(hidden_layer_sizes=(100,200,200),early_stopping=False)

from sklearn.model_selection import GridSearchCV
# params_dict = {'C':[0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01,0.03,0.05,0.1,0.3,0.5,1.0,3.0,5.0,10,30,50], }
params_dict = {'C':[20,30,40,50],
               'gamma':[0.001,0.003,0.005,0.01,0.05,1]}

# params_dict = {'C':np.linspace(0.1,2,20)}
# params_dict = {'n_components':np.arange(1,48)}

# RandomForest params
# params_dict = {'n_estimators':[70],
#                'max_depth':[9],
#                'max_features':[10,20,30,40,50,60,70,80]}
# SVM GRID RBF
# params_dict = {'C':np.linspace(10,30,num=30),
#                'gamma':np.linspace(0.005,0.007,num=2)}

grid = GridSearchCV(model,param_grid=params_dict,scoring = 'f1' ,n_jobs=6,cv=4,verbose=1)

grid = model
#
grid.fit(dataset.train[0],dataset.train[1])
#
# print(grid.best_params_)

model_scorer(dataset = dataset, model = grid)


