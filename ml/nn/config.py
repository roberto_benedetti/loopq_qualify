import argparse, os
import __root__
from datetime import datetime

def str2bool(v):
  return v.lower() in ('true', '1')

arg_lists = []
parser = argparse.ArgumentParser()

def add_argument_group(name):
  arg = parser.add_argument_group(name)
  arg_lists.append(arg)
  return arg


# Data
data_arg = add_argument_group('Data')
# Training / test parameters
train_arg = add_argument_group('Training')

train_arg.add_argument('--num_epochs', type=int, default=100, help='max num of epochs')
train_arg.add_argument('--batch_size', type=int, default=5, help='batch size')
train_arg.add_argument('--test_batch_size', type=int, default=10, help='test batch size')
train_arg.add_argument('--checkpoint_folder', type=str, default='checkpoints/', help='where to store computed models')

train_arg.add_argument('--weight_decay',type=float,default=1e-5)

# Misc
misc_arg = add_argument_group('Misc')

misc_arg.add_argument('--gpu_memory_fraction', type=float, default=1.0)


def get_config():
  config, unparsed = parser.parse_known_args()
  return config, unparsed
