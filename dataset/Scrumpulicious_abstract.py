'''Base Class for dataset builder, concrete classes re-implements the methods of template_processer()'''

import __root__
import numpy as np
import pandas as pd
import os
import MLlib
from ml.nn.utils import set_random_numpy
from dataset.Raw_reader import Scrumpoulicious
# Fix random generators for reproducibility
set_random_numpy()

# Set and create save folders

class Scrumpulicious_abstract():
    def __init__(self, dataset_name:str):
        print('-'*(len(dataset_name)+4+9))
        print('--DATASET: {}--'.format(dataset_name))
        print('-'*(len(dataset_name)+4+9))

        self.dataset_name = dataset_name
        self.SAVE_FOLDER = '{}/'.format(dataset_name)

        # make folders
        if not os.path.exists(self.SAVE_FOLDER):
            os.makedirs(self.SAVE_FOLDER)

        if not os.path.exists(os.path.join(self.SAVE_FOLDER,'train')):
            os.makedirs(os.path.join(self.SAVE_FOLDER,'train'))

        if not os.path.exists(os.path.join(self.SAVE_FOLDER,'test')):
            os.makedirs(os.path.join(self.SAVE_FOLDER,'test'))

        # Load raw data
        self.df = Scrumpoulicious().df

        print('DATASET SHAPE:',self.df.shape)

    def template_processer(self):
        print('**TEMPLATE PROCESSER**')
        print('** [1/3]\tData Preprocessing**')
        self.data_preprocesser()
        print('** [2/3]\tData Random Splitting**')
        self.data_random_split()
        print('Training_shape:',self.df_tr.shape)
        print('Test_shape:',self.df_te.shape)
        print('** [3/3]\tSaving dataset**')
        self.save_dataset()

    def data_preprocesser(self):
        # We should remove High collinear features as seen in data_explorer

        corr_abs = self.df.corr().abs()

        upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
        print('Rows to remove due to high correlation (>.85)',to_drop)
        #
        self.df = self.df.drop(self.df[to_drop], axis=1)
        print('New dataframe shape:',self.df.shape)
    def data_random_split(self):
        # compute a random training test split

        self.df = MLlib.shuffleDataframe(self.df)
        df_tr,df_te = MLlib.splitDataframe(self.df,.7)

        print(df_tr.head())

        print('DATASET SPECS:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(self.df.shape,sum(self.df.GREEN),
                                                       100*sum(self.df.GREEN)/len(self.df),
                                                       len(self.df)-sum(self.df.GREEN),
                                                       100*(1-sum(self.df.GREEN)/len(self.df))))

        print('TRAINING_DATASET SPECS:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df_tr.shape,sum(df_tr.GREEN),
                                                       100*sum(df_tr.GREEN)/len(df_tr),
                                                       len(df_tr)-sum(df_tr.GREEN),
                                                       100*(1-sum(df_tr.GREEN)/len(df_tr))))

        print('TEST_DATASET SPECS:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df_te.shape,sum(df_te.GREEN),
                                                       100*sum(df_te.GREEN)/len(df_te),
                                                       len(df_te)-sum(df_te.GREEN),
                                                       100*(1-sum(df_te.GREEN)/len(df_te))))

        self.df_tr = df_tr
        self.df_te = df_te


    def save_dataset(self):
        # Store data
        self.df_tr.to_csv(os.path.join(self.SAVE_FOLDER,'train','train.csv'), sep=';', index=None)
        self.df_te.to_csv(os.path.join(self.SAVE_FOLDER,'test','test.csv'), sep=';', index=None)