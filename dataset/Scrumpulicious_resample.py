'''Create simple data model for classical analysis'''

import numpy as np
import pandas as pd
from ml.nn.utils import set_random_numpy
from imblearn.over_sampling import SMOTE
import MLlib
from dataset.Scrumpulicious_abstract import Scrumpulicious_abstract

# Fix random generators for reproducibility
set_random_numpy()

class Scrumpulicious_resample(Scrumpulicious_abstract):
    def __init__(self):
        super(Scrumpulicious_resample,self).__init__(dataset_name='Scrumpulicious_resample')

    def data_random_split(self):
        super().data_random_split()
        # perform resampling of minor distribution
        sampler = SMOTE(sampling_strategy=.3,random_state=999)

        print(self.df_tr)
        x_cols = list(self.df_tr.columns)
        x_cols.remove('GREEN')
        x_cols.remove('TIME')
        df_tr_x = self.df_tr[x_cols]
        df_tr_y = self.df_tr['GREEN']

        # print(df_tr_y.value_counts(), np.bincount(df_tr_y))

        print('TRAINING_DATASET SPECS BEFORE RESAMPLING:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df_tr_y.shape,sum(df_tr_y),
                                                       100*sum(df_tr_y)/len(df_tr_y),
                                                       len(df_tr_y)-sum(df_tr_y),
                                                       100*(1-sum(df_tr_y)/len(df_tr_y))))
        df_tr_x_new,df_tr_y_new = sampler.fit_resample(df_tr_x.values, df_tr_y.values)

        print('TRAINING_DATASET SPECS AFTER RESAMPLING:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df_tr_y_new.shape,sum(df_tr_y_new),
                                                       100*sum(df_tr_y_new)/len(df_tr_y_new),
                                                       len(df_tr_y_new)-sum(df_tr_y_new),
                                                       100*(1-sum(df_tr_y_new)/len(df_tr_y_new))))
        df_tr_x_new = pd.DataFrame(df_tr_x_new,columns=x_cols)
        df_tr_y_new= pd.DataFrame(df_tr_y_new,columns=['GREEN'])
        # print(df_tr_y_new.values.value_counts(), np.bincount(df_tr_y_new.values))
        df_tr = pd.concat([df_tr_x_new,df_tr_y_new],axis=1)

        # Shuffle dataframe again
        df_tr = MLlib.shuffleDataframe(df_tr)

        print()
        self.df_tr = df_tr
        # self.df_te
