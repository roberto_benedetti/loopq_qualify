import pandas as pd
import numpy as np
from sklearn.metrics import f1_score, accuracy_score
from sklearn.metrics import confusion_matrix

from MLlib import plot_confusion_matrix, model_scorer
from data_loader.Scrumpulicious import Scrumpulicious_linear

from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC,SVC
from sklearn.neural_network import MLPClassifier
import matplotlib.pyplot as plt
from ml.nn.utils import set_random_numpy

set_random_numpy()


dataset = Scrumpulicious_linear()
dataset.read()

# pca = PCA(n_components=50)
#
# pca.fit(dataset.train[0])
# dataset.train[0] = pca.transform(dataset.train[0])
# dataset.test[0]  = pca.transform(dataset.test[0])

print(dataset.train[0].shape)

# model = LogisticRegression(C=30,penalty='l1', class_weight={0:0.7,1:1})
# model = LinearSVC( C=3,class_weight={0:0.7,1:1})
# model = SVC(gamma=0.003, C=50, class_weight={0:0.7,1:1})
# model = RandomForestClassifier(max_features=28,criterion='entropy',max_depth=10,n_estimators=60,class_weight={0:0.7,1:1},n_jobs=-1,random_state=555)
# model = LinearDiscriminantAnalysis()
# model = QuadraticDiscriminantAnalysis()
# model = MLPClassifier(hidden_layer_sizes=(100,200,200),early_stopping=True)


# SECONDO MODELLO PER CROSSVAL RANDOM PER SPLITTING DATI SETTATO A 999
model = RandomForestClassifier(max_features=20,criterion='entropy',max_depth=10,n_estimators=70,class_weight={0:0.7,1:1},n_jobs=-1,random_state=555)

from sklearn.model_selection import GridSearchCV
# params_dict = {'C':[0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01,0.03,0.05,0.1,0.3,0.5,1.0,3.0,5.0,10,30,50],
#                'gamma':[0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01,0.03,0.05,0.1,0.3,0.5,1.0,3.0,5.0,10,30,50]}
# params_dict = {'C':[3.0,5.0,10,15,20,30,40,50,60,70],'penalty':['l1','l2']}

# params_dict = {'C':[0.5,1.0,3.0,5.0,10,30,50]}
# params_dict = {'n_components':np.arange(1,48)}

# RandomForest params
# params_dict = {'max_features':[28],
#                'max_depth': [7,8,9,10,11],
#                'n_estimators':[50,60,70,80,90,100]
#                }
# params_dict = {'random_state':[555,1,2,3,4,5,6],
#                'max_depth': [7,8,9,10,11],
#                'n_estimators':[50,60,70,80,90,100]
#                }
params_dict = {'random_state':[555],
               # 'max_features': np.arange(2,48,1),
               # 'max_depth': [7,8,9,10,11],
               'n_estimators':[50,60,70,80,90]
               }
# SVM GRID RBF
# params_dict = {'C':np.linspace(10,30,num=30),
#                'gamma':np.linspace(0.005,0.007,num=2)}

grid = GridSearchCV(model,param_grid=params_dict,scoring = 'f1' ,n_jobs=-1,cv=4,verbose=1)
#
grid = model

grid.fit(dataset.train[0],dataset.train[1])
#
# print(grid.best_params_)

model_scorer(dataset = dataset, model = grid)


