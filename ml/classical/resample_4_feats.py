import pandas as pd
import numpy as np
from sklearn.metrics import f1_score, accuracy_score
# from sklearn.metrics.scorer import f1_scorer
from MLlib import model_scorer
from data_loader.Scrumpulicious import Scrumpulicious_resample_4

from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC,SVC
from sklearn.neural_network import MLPClassifier

from ml.nn.utils import set_random_numpy

set_random_numpy()

dataset = Scrumpulicious_resample_4()
dataset.read()


# pca = PCA(n_components=50)
#
# pca.fit(dataset.train[0])
# dataset.train[0] = pca.transform(dataset.train[0])
# dataset.test[0]  = pca.transform(dataset.test[0])

print(dataset.train[0].shape)

# model = LogisticRegression(class_weight={0:0.3,1:.7})
# model = LogisticRegressionCV(cv = 10 , scoring='f1', verbose=1, n_jobs=-1)
# model = LinearSVC()
# model = LinearSVC(class_weight={0:0.3,1:.7})
# model = SVC(gamma='auto',class_weight={0:0.4,1:.6})
model = RandomForestClassifier(n_estimators= 90, max_depth=13,max_features=4,criterion='entropy',class_weight={0:.7,1:1.0},n_jobs=-1,random_state=555)
# model = LinearDiscriminantAnalysis()
# model = QuadraticDiscriminantAnalysis()
# model = MLPClassifier(hidden_layer_sizes=(100,200,200),early_stopping=False,verbose=1)

#BEST SO FAR .6349
# model = RandomForestClassifier(max_features=48,criterion='entropy',max_depth=4,n_estimators=73,class_weight={0:0.3,1:.7},n_jobs=-1,random_state=555)


from sklearn.model_selection import GridSearchCV
# params_dict = {'C':[0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01,0.03,0.05,0.1,0.3,0.5,1.0,3.0,5.0,10,30,50]}

# params_dict = {'C':[0.5,1.0,3.0,5.0,10,30,50]}
# params_dict = {'n_components':np.arange(1,48)}

# RandomForest params
# params_dict = {'max_features':[1,2,3,4],
#                'max_depth':[4,5,6,7,8,9,10],
#                'n_estimators':[75]
#                }
params_dict = {
               'max_depth':[9,10,11,12,13],
               'n_estimators':[90,100,110]
               }
# SVM GRID RBF
# params_dict = {'C':[0.01,0.03,0.05,0.1,0.5,1.0,]#,
               #'gamma':[5.0,7,10,20,30,40,50,60]
               # }
grid = GridSearchCV(model,param_grid=params_dict,scoring = 'f1' ,n_jobs=-1,cv=4,verbose=1)

# grid = model
#
grid.fit(dataset.train[0],dataset.train[1])
#
print(grid.best_params_)

model_scorer(dataset = dataset, model = grid)


