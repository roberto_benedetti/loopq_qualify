import torch
import numpy
from sklearn.metrics import f1_score
from tensorboardX import SummaryWriter
from torch.optim.lr_scheduler import ReduceLROnPlateau, StepLR

from ml.nn.dataset import dataset_scrumpulicious_linear, dataset_scrumpulicious_resample, \
    dataset_scrumpulicious_linear_4, dataset_scrumpulicious_time_collector
from ml.nn.model.nn_linear_4 import nn_linear_4
from ml.nn.model.nn_time_collector import nn_time_collector
from ml.nn.model.nn_linear import nn_linear
from ml.nn.utils import set_random_numbers


# from torch.optim.lr_scheduler import StepLR

from torch.utils.data import DataLoader
from torch import nn
import torch
from torch.autograd import Variable

import time, os
import numpy as np
import random
import datetime
# warning suppresser
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
# Set random numbers for reproducibility
set_random_numbers(np, torch, random)

class Trainer():
    def __init__(self, config):
        self.config = config
        if self.config.dataset == 'classical':
            self.model=nn_linear().cuda()
        elif self.config.dataset =='linear_4':
            self.model=nn_linear_4().cuda()
        elif self.config.dataset == 'time_collector':
            self.model = nn_time_collector().cuda()

        # self.criterion = nn.CrossEntropyLoss()
        # For unbalanced data use weighted loss
        self.criterion = nn.CrossEntropyLoss(weight=torch.Tensor([0.1,1.0]).cuda())


        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=config.learning_rate, momentum=.9
                                         , weight_decay = self.config.weight_decay)
        # self.scheduler = ReduceLROnPlateau(self.optimizer, 'min', patience=10, verbose=True)
        self.scheduler = StepLR(self.optimizer, 150, gamma=0.1)
        self.plot=False
        self.prepare_data()
        # add tensorboard for logging
        self.writer = SummaryWriter(log_dir='runs/'+self.config.log_name+'_'+str(datetime.datetime.today()))
        self.softmax = nn.LogSoftmax(dim=1)
    def prepare_data(self):

        if self.config.dataset == 'classical':
            dataset = dataset_scrumpulicious_linear
        elif self.config.dataset == 'linear_4':
            dataset = dataset_scrumpulicious_linear_4

        elif self.config.dataset == 'time_collector':
            dataset = dataset_scrumpulicious_time_collector
        else:
            dataset = dataset_scrumpulicious_resample

        training_dataset = dataset(is_train=True)
        test_dataset    = dataset(is_train=False)
        training_dataset.read_data()
        test_dataset.read_data()


        training_dataset.print_info()

        self.train_dataloader = DataLoader(training_dataset, batch_size=self.config.batch_size,
                                           shuffle=True, num_workers=8, drop_last=True)
        self.test_dataloader  = DataLoader(test_dataset, batch_size=self.config.batch_size,
                                           shuffle=False, num_workers=8, drop_last=True)

        # init checkpoint folder for storing computed models

        # if not os.path.exists(self.config.checkpoint_folder):
        #     os.makedirs(self.config.checkpoint_folder)

    def epoch_train_step(self,epoch):
            self.model.train()

            # self.scheduler.step()
            # for param_group in self.optimizer.param_groups: print(param_group['lr'])
            batch_idx = 0
            train_loss = 0
            for batch_idx, batch in enumerate(self.train_dataloader, 0):
                batch_x = Variable(batch[0]).cuda()
                batch_y = Variable(batch[1]).cuda()

                # ===================forward=====================

                output = self.model(batch_x)
                loss = self.criterion(output,batch_y)

                # ===================backward====================
                self.optimizer.zero_grad()
                loss.backward()
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), 10)
                self.optimizer.step()

                # Update training loss
                train_loss += loss.item()

            train_loss = train_loss / (batch_idx + 1)
            return train_loss

    def start_train(self):
        # Init val loss for storing best model
        best_val_loss = np.inf

        # Loop over Epochs
        for epoch in range(self.config.num_epochs):
            # for name, param in self.model.named_parameters():
            #     if 'bn' not in name:
            #         self.writer.add_histogram(name, param, epoch)
            # self.plot_kernels(epoch)
            epoch_start_time = time.time()

            train_loss=self.epoch_train_step(epoch)
            val_loss_tr, f1_tr = self.evaluate(epoch, is_train=True)
            val_loss_te, f1_te = self.evaluate(epoch, is_train=False)
            # val_loss = 0
            # self.scheduler.step()
            print('| end of epoch [{}/{}] | time: {:5.2f}s | train loss: {:5.7f} | f1_tr {:5.7f} | valid loss te {:5.7f} | f1_te {:5.7f}  '
                  .format(epoch,self.config.num_epochs, (
                    time.time() - epoch_start_time),train_loss,f1_tr, val_loss_te, f1_te))
            self.writer.add_scalar('train_loss', train_loss, epoch)
            self.writer.add_scalar('valid_loss', val_loss_te, epoch)
            self.writer.add_scalar('f1_score_test', f1_te, epoch)

            # if best save model
            # if val_loss_te < best_val_loss:
            #     best_val_loss = val_loss_te
            #     torch.save(self.model.encoder, os.path.join(self.config.checkpoint_folder,
            #                                                 'autoencoder_epoch_{}_val_te_{:5.4f}.pth'.format(epoch,
            #                                                                                             val_loss_te)))

    def evaluate(self,epoch, is_train:bool):
        # evaluate results on train/test set
        if is_train:
            target = self.train_dataloader
            label = 'training'
        else:
            target = self.test_dataloader
            label = 'test'

        batch_idx=0
        true_y = []
        outputs = []
        f1_scores  = []
        # turn on evaluation mode
        self.model.eval()

        with torch.no_grad():
            total_loss = 0
            # loop over test dataset
            for batch_idx, batch in enumerate(target, 0):
                batch_x = Variable(batch[0]).cuda()
                batch_y = Variable(batch[1]).cuda()

                output = self.model(batch_x)
                loss = self.criterion(output,batch_y)
                total_loss += loss.item()

                true_y.append(batch_y.data.cpu().numpy())
                outputs.append(np.argmax(self.softmax(output).data.cpu().numpy(),axis=1))

            val_loss = total_loss / (batch_idx + 1)
            # Flatten lists
            true_y = [item for sublist in true_y for item in sublist]
            outputs = [item for sublist in outputs for item in sublist]
            # Compute f1 score
            f1 = f1_score(y_true=true_y,y_pred = outputs)

        return val_loss, f1
