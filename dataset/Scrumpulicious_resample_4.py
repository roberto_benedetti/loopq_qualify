'''Augment data only to most relevant features'''

import numpy as np
import pandas as pd

from imblearn.over_sampling import SMOTE

import MLlib
from dataset.Scrumpulicious_abstract import Scrumpulicious_abstract
from ml.nn.utils import set_random_numpy

# Fix random generators for reproducibility
set_random_numpy()

class Scrumpulicious_resample_4(Scrumpulicious_abstract):
    def __init__(self):
        super(Scrumpulicious_resample_4,self).__init__(dataset_name='Scrumpulicious_resample_4')


    def data_preprocesser(self):
        # We should remove High collinear features as seen in data_explorer

        corr_abs = self.df.corr().abs()

        upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
        print('Rows to remove due to high correlation (>.85)',to_drop)
        #
        self.df = self.df.drop(self.df[to_drop], axis=1)
        print('New dataframe shape:',self.df.shape)

        high_correlated = list(corr_abs['GREEN'][corr_abs['GREEN'] > 0.08].index)

        self.df = self.df[high_correlated]

    def data_random_split(self):
        super().data_random_split()
        # perform resampling of minor distribution
        sampler = SMOTE(sampling_strategy=.3,random_state=999)

        print(self.df_tr)
        x_cols = list(self.df_tr.columns)
        x_cols.remove('GREEN')
        # x_cols.remove('TIME')
        df_tr_x = self.df_tr[x_cols]
        df_tr_y = self.df_tr['GREEN']

        # print(df_tr_y.value_counts(), np.bincount(df_tr_y))

        print('TRAINING_DATASET SPECS BEFORE RESAMPLING:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df_tr_y.shape,sum(df_tr_y),
                                                       100*sum(df_tr_y)/len(df_tr_y),
                                                       len(df_tr_y)-sum(df_tr_y),
                                                       100*(1-sum(df_tr_y)/len(df_tr_y))))
        df_tr_x_new,df_tr_y_new = sampler.fit_resample(df_tr_x.values, df_tr_y.values)

        print('TRAINING_DATASET SPECS AFTER RESAMPLING:\n\tSHAPE:\t{}\n\t# POSITIVE:\t{}\t({:2.1f}%)'
              '\n\t# NEGATIVE:\t{}\t({:2.1f}%)'.format(df_tr_y_new.shape,sum(df_tr_y_new),
                                                       100*sum(df_tr_y_new)/len(df_tr_y_new),
                                                       len(df_tr_y_new)-sum(df_tr_y_new),
                                                       100*(1-sum(df_tr_y_new)/len(df_tr_y_new))))
        df_tr_x_new = pd.DataFrame(df_tr_x_new,columns=x_cols)
        df_tr_y_new= pd.DataFrame(df_tr_y_new,columns=['GREEN'])
        # print(df_tr_y_new.values.value_counts(), np.bincount(df_tr_y_new.values))
        df_tr = pd.concat([df_tr_x_new,df_tr_y_new],axis=1)

        # Shuffle dataframe again
        df_tr = MLlib.shuffleDataframe(df_tr)

        print()
        self.df_tr = df_tr
        # self.df_te
