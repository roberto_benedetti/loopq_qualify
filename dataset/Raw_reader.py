'''Base class for loading raw data in pandas dataframe'''

import __root__
import numpy as np
import pandas as pd
import os
from ml.nn.utils import set_random_numpy

# Fix random generators for reproducitbility
set_random_numpy()

path = os.path.join(__root__.path(),'dataset','raw','Scrumpulicious.csv')

class Scrumpoulicious():
    def __init__(self):
        df = pd.read_csv(path)
        df.TIME = pd.to_datetime(df.TIME)
        self.df = df