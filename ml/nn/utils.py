import numpy as np
import torch
import random

def set_random_numbers(numpy,torch,random,seed=555):
    numpy.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    random.seed(seed)

    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    print('random values set')
# seed=1111 -> seed for most non time daset
#seed = 2 -> seed for time dataset
def set_random_numpy(seed:int=1111):
    np.random.seed(seed)