import os
import torch
from datetime import datetime
import numpy as np
from joblib import delayed, Parallel
from sklearn.preprocessing import minmax_scale
from statsmodels.tsa.stattools import pacf
from torch import nn

import __root__
from torch.utils.data import Dataset
import pandas as pd
import random, copy
import scipy.signal as sc
import matplotlib

from ml.nn.sample_type import Sample

matplotlib.use('Agg')
from data_loader.Scrumpulicious import Scrumpulicious_linear, Scrumpulicious_resample, Scrumpulicious_linear_4, \
    Scrumpulicious_time_collector
import matplotlib.pyplot as plt


class dataset_scrumpulicious_linear(Dataset):

    def __init__(self, is_train, train_size:int=1024, test_size:int=1024):
        self.dataset = Scrumpulicious_linear()

        self.is_train = is_train
        self.train_size = train_size
        self.test_size = test_size

        self.training_set = []
        self.test_set = []


    def read_data(self):
        self.dataset.read()

        if self.is_train:
            data = (torch.FloatTensor(self.dataset.train[0]), torch.FloatTensor(self.dataset.train[1]))
        else:
            data= (torch.FloatTensor(self.dataset.test[0]), torch.FloatTensor(self.dataset.test[1]))

        # (torch.FloatTensor(self.dataset.train[0]), torch.FloatTensor(self.dataset.train[1]))

        for i in range(0, data[0].size(0)):

            if self.is_train:

                self.training_set.append(Sample(data[0][i], data[1][i]))
            else:
                self.test_set.append(Sample(data[0][i], data[1][i]))

        #
        # if self.is_train:
        #     self.training_set = (torch.FloatTensor(self.dataset.train[0]), torch.FloatTensor(self.dataset.train[1]))
        # else:
        #     self.test_set = (torch.FloatTensor(self.dataset.test[0]), torch.FloatTensor(self.dataset.test[1]))
        #

        # self.dataset =None

    def __len__(self):
        if self.is_train:
            return len(self.training_set)
        else:
            return len(self.test_set)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        # """
        if self.is_train:
            input, target = self.training_set[index].read_features(), self.training_set[index].read_labels().long()

            # input, target = (self.training_set[0])[index], (self.training_set[1])[index].long()
        else:
            # input, target = (self.test_set[0])[index], (self.test_set[1])[index].long()
            input, target = self.test_set[index].read_features(), self.test_set[index].read_labels().long()

        return input, target
        # """


    def print_info(self):
        print('--------------------------------------')
        print('------Dataset Info--------')
        print('Dataset Name: {}'.format(self.__class__.__name__))
        if self.is_train:
            print('\tNumber of Training samples: {}'.format(len(self.training_set)))
            print('\tNumber of Training features: {}'.format(len(self.training_set[0].read_features())))


        else:
            print('Number of Test samples: {}'.format(len(self.test_set)))


class dataset_scrumpulicious_resample(Dataset):

    def __init__(self, is_train, train_size:int=1024, test_size:int=1024):
        self.dataset = Scrumpulicious_resample()

        self.is_train = is_train
        self.train_size = train_size
        self.test_size = test_size

        self.training_set = []
        self.test_set = []


    def read_data(self):
        self.dataset.read()

        if self.is_train:
            data = (torch.FloatTensor(self.dataset.train[0]), torch.FloatTensor(self.dataset.train[1]))
        else:
            data= (torch.FloatTensor(self.dataset.test[0]), torch.FloatTensor(self.dataset.test[1]))

        # (torch.FloatTensor(self.dataset.train[0]), torch.FloatTensor(self.dataset.train[1]))

        for i in range(0, data[0].size(0)):

            if self.is_train:

                self.training_set.append(Sample(data[0][i], data[1][i]))
            else:
                self.test_set.append(Sample(data[0][i], data[1][i]))

        #
        # if self.is_train:
        #     self.training_set = (torch.FloatTensor(self.dataset.train[0]), torch.FloatTensor(self.dataset.train[1]))
        # else:
        #     self.test_set = (torch.FloatTensor(self.dataset.test[0]), torch.FloatTensor(self.dataset.test[1]))
        #

        # self.dataset =None

    def __len__(self):
        if self.is_train:
            return len(self.training_set)
        else:
            return len(self.test_set)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, target) where target is index of the target class.
        """
        # """
        if self.is_train:
            input, target = self.training_set[index].read_features(), self.training_set[index].read_labels().long()

            # input, target = (self.training_set[0])[index], (self.training_set[1])[index].long()
        else:
            # input, target = (self.test_set[0])[index], (self.test_set[1])[index].long()
            input, target = self.test_set[index].read_features(), self.test_set[index].read_labels().long()

        return input, target
        # """


    def print_info(self):
        print('--------------------------------------')
        print('------Dataset Info--------')
        print('Dataset Name: {}'.format(self.name))
        if self.is_train:
            print('Number of Training samples: {}'.format(len(self.training_set)))

        else:
            print('Number of Test samples: {}'.format(len(self.test_set)))

class dataset_scrumpulicious_linear_4(dataset_scrumpulicious_linear):
    def __init__(self, is_train:bool):
        super(dataset_scrumpulicious_linear_4,self).__init__(is_train=is_train)
        self.dataset = Scrumpulicious_linear_4()

class dataset_scrumpulicious_time_collector(dataset_scrumpulicious_linear):
    def __init__(self, is_train:bool):
        super(dataset_scrumpulicious_time_collector,self).__init__(is_train=is_train)
        self.dataset = Scrumpulicious_time_collector()

