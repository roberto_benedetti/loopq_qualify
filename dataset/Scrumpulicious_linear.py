'''Create simple data model for classical analysis'''

import numpy as np

# Fix random generators for reproducitbility
from dataset.Scrumpulicious_abstract import Scrumpulicious_abstract
from ml.nn.utils import set_random_numpy

set_random_numpy()

# Set and create save folders

class Scrumpulicious_linear(Scrumpulicious_abstract):
    def __init__(self):
        super(Scrumpulicious_linear,self).__init__(dataset_name='Scrumpulicious_linear')


    def data_preprocesser(self):
        # We should remove High collinear features as seen in data_explorer

        corr_abs = self.df.corr().abs()

        upper = corr_abs.where(np.triu(np.ones(corr_abs.shape), k=1).astype(np.bool))
        to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]
        print('Rows to remove due to high correlation (>.85)',to_drop)
        #
        self.df = self.df.drop(self.df[to_drop], axis=1)
        print('New dataframe shape:',self.df.shape)