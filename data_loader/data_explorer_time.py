''' Simple script for analyzing time related dataset '''
from sklearn.manifold import TSNE

import MLlib
import __root__
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt
import seaborn as sns


# from data_loader.Scrumpulicious import Scrumpulicious_time_collector

# dataset = Scrumpulicious_time_collector()
# dataset.read()

PATH = os.path.join(__root__.path(), 'dataset', 'Scrumpulicious_time_collector')


df_tr = pd.read_csv(os.path.join(PATH, 'train', 'train.csv'), sep=';')
df_te = pd.read_csv(os.path.join(PATH, 'test', 'test.csv'), sep=';')

df = pd.concat([df_tr,df_te],axis=0)

corr_cols = [x for x in df.columns if '_' in x]# # correlation of new time features
corr_cols.append('GREEN') # also add label

# Plot new Corr
corr = df[corr_cols].corr()

corr_abs = df.corr().abs()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))
plt.title('New features matrix correlations')

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.show()

# We see that we added few new features with some correlation with the output label, this might improve model fitting

# Lets plot the entire corr matrix for visualization
# Plot new Corr
corr = df.corr()

corr_abs = df.corr().abs()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))
plt.title('New dataset full matrix correlations')

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.show()

# notime =  list(df.columns)
# notime.remove('TIME')
# df = df[notime]
# df = (df-df.mean())/df.std()
# # Make a boxplot
# plt.figure()
# plt.title('boxplot')
# df.boxplot()
# plt.show()

# We want to see if there are linear correlations, let's take the most corr feats with output label

high_correlated = list(corr_abs['GREEN'][corr_abs['GREEN'] > 0.05].index)
print(high_correlated)

# Let's try a pairplot again
df = df[high_correlated]
sns.set(style="ticks", color_codes=True)
g = sns.pairplot(df,hue="GREEN",diag_kind='kde' )
plt.show()

# We can't really see some linear dependencies, or separabilities from these
cols = list(df.columns)
y_cols = ['GREEN']
remove_cols = ['TIME']

x_cols = [x for x in cols if x not in y_cols]
x_cols = [x for x in x_cols if x not in remove_cols]
# Let's try the t-sne
# to improve visualization we collect as many green samples we can, while we resample only a fraction of gold samples


df_green = df.loc[df['GREEN']==1]
df_gold = df.loc[df['GREEN']==0]

df_gold = MLlib.shuffleDataframe(df_gold)
df_gold = df_gold[0:200]

df = pd.concat([df_gold,df_green], axis=0)
df = MLlib.shuffleDataframe(df)

print('RE-Sampled dataframe for t-sne visualization:',df.shape)

green = df['GREEN'] == 1
gold = df['GREEN'] == 0
df_x = df[x_cols]
print('number of positives:', sum(green))
tsne = TSNE(n_components=2,verbose=1, random_state=999, perplexity=40 ,  )
x_embedded = tsne.fit_transform(X=df_x)

fig=plt.figure()
plt.title('T-SNE visualization')
ax = fig.add_subplot(111)
# ax = fig.add_subplot(111, projection='3d')
plt.scatter(x_embedded[green,0],x_embedded[green,1], color = 'green', label ='positive')
plt.scatter(x_embedded[gold,0],x_embedded[gold,1], color = 'yellow', label ='negative')
plt.legend()
plt.show()

print()
