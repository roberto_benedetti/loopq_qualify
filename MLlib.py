from sklearn.metrics import confusion_matrix
import numpy as np

from ml.nn.utils import set_random_numpy
import matplotlib.pyplot as plt
set_random_numpy()
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import f1_score, accuracy_score
from sklearn.metrics import confusion_matrix

def shuffleDataframe(df):
    df = df.iloc[np.random.permutation(len(df))]
    df = df.reset_index(drop=True)
    return df

def splitDataframe(df,i):
    N = df.shape[0]
    tr_size = int(N*i)
    test_size = N - tr_size
    df_tr = df.iloc[0:tr_size]
    df_te = df.iloc[tr_size:]
    df_te.index = range(test_size)
    return (df_tr , df_te)

def mapperTrueFalse(arr):
    x = arr*-1
    ones= np.ones(len(x))
    ones[x==-1] = 0
    return ones

def classAccuracy(y_true,y_pred):
    cnf_matrix = confusion_matrix(y_true, y_pred)
    acc0 = cnf_matrix[0][0] / sum(cnf_matrix[0])
    acc1 = cnf_matrix[1][1] / sum(cnf_matrix[1])
    # return {'acc_0':acc0,'acc_1':acc1}
    return acc0,acc1

def normalizeDf(X):
    return (X-X.mean())/X.std()

def plot_confusion_matrix(y_true, y_pred,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = unique_labels(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    # print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

def model_scorer(dataset,model):
    '''Computes various scores'''
    tr_prediction = model.predict(dataset.train[0])
    te_prediction = model.predict(dataset.test[0])
    print('ACCURACY SCORES')
    print('\ttr:\t{:2.4f}'.format(accuracy_score(y_true=dataset.train[1],y_pred=tr_prediction)))
    print('\tte:\t{:2.4f}'.format(accuracy_score(y_true=dataset.test[1] , y_pred=te_prediction)))
    print('F1 SCORES')
    print('\ttr:\t{:2.4f}'.format(f1_score(pos_label=1,average='macro',y_true=dataset.train[1],y_pred=tr_prediction)))
    print('\tte:\t{:2.4f}'.format(f1_score(pos_label=1,average='macro',y_true=dataset.test[1] , y_pred=te_prediction)))

    # class accuracy
    print('CLASS_ACCURACY TEST')
    cm = confusion_matrix(y_true=dataset.test[1], y_pred=te_prediction)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    class_acc = cm.diagonal()
    print('\tclass_0: {:2.4f}\tclass_1: {:2.4f}'.format(class_acc[0],class_acc[1]))

    plot_confusion_matrix(y_true=dataset.test[1],y_pred=te_prediction,title='Confusion matrix test prediction')
    plt.show()

